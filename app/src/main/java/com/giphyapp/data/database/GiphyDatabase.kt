package com.giphyapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.giphyapp.data.database.dao.GiphyDao
import com.giphyapp.data.database.entity.GiphyEntity

const val DATABASE_VERSION = 1

@Database(
    version = DATABASE_VERSION,
    entities = [
        GiphyEntity::class
    ],
    exportSchema = true
)

abstract class GiphyDatabase : RoomDatabase() {

    companion object {
        internal const val NAME = "giphy.db"
    }

    abstract fun getGiphyDao(): GiphyDao
}