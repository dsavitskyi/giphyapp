package com.giphyapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.giphyapp.data.database.entity.DownloadStatus
import com.giphyapp.data.database.entity.GiphyEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GiphyDao {

    @Query("SELECT * FROM giphy WHERE download_status=:status")
    suspend fun getAllGifsByStatus(status: DownloadStatus): List<GiphyEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(action: GiphyEntity)

    @Query("UPDATE giphy SET gif_uri=:uri, gif_name=:gifName, download_status=:status WHERE download_id = :downloadId")
    suspend fun updateGifDownloadStatus(uri: String, gifName: String, status: DownloadStatus, downloadId: Long)

    @Query("UPDATE giphy SET download_status=:status WHERE _id = :gifId")
    suspend fun setGifSDeleteStatus(gifId: String, status: DownloadStatus = DownloadStatus.DELETED)

    @Query("SELECT * FROM giphy WHERE download_status=:status")
    fun getAllDownloadedGifs(status: DownloadStatus = DownloadStatus.DOWNLOADED): Flow<List<GiphyEntity>>
}