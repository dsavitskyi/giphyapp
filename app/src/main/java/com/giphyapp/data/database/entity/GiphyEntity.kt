package com.giphyapp.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "giphy")
data class GiphyEntity constructor(
    @PrimaryKey
    @ColumnInfo(name = "_id")
    val gifId: String,

    @ColumnInfo(name = "download_id")
    val downloadId: Long,

    @ColumnInfo(name = "gif_name")
    val gifName: String = "",

    @ColumnInfo(name = "gif_url")
    val gifUrl: String,

    @ColumnInfo(name = "gif_uri")
    val gifUri: String = "",

    @ColumnInfo(name = "download_status")
    val downloadStatus: DownloadStatus
)

enum class DownloadStatus {
    PENDING,
    DOWNLOADED,
    DELETED
}