package com.giphyapp.data.datasource.remote

import com.giphyapp.data.datasource.model.GifResponseBody
import com.giphyapp.data.network.IApiProvider
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject

class GiphyRemoteDataSource @Inject constructor(apiProvider: IApiProvider) {

    private val giphyApi = apiProvider.getApi(GIPHY_API_URL, GiphyApi::class.java)

    suspend fun getGifPackage(searchQuery: String, offset: Int, limit: Int): GifResponseBody {
        return giphyApi.getGifPackage(query = searchQuery, offset = offset, limit = limit)
    }

    interface GiphyApi {
        @GET("v1/gifs/search")
        suspend fun getGifPackage(
            @Query("api_key") apiKey: String = GIF_API_KEY,
            @Query("q") query: String,
            @Query("offset") offset: Int,
            @Query("limit") limit: Int,
            @Query("rating") rating: String = "g",
            @Query("lang") lang: String = "en",
        ): GifResponseBody
    }

    companion object {
        private const val GIF_API_KEY = "IaLTOv3cOn9YHQYeZuF9miii6YWtsBAI"
        private const val GIPHY_API_URL = "https://api.giphy.com/"
    }
}