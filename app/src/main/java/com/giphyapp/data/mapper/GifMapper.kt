package com.giphyapp.data.mapper

import com.giphyapp.data.database.entity.DownloadStatus
import com.giphyapp.data.database.entity.GiphyEntity
import com.giphyapp.data.datasource.model.GifResponseBody
import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.model.GifDownloadModel
import com.giphyapp.domain.model.GifModel
import com.giphyapp.domain.model.DownloadStatus as DomainDownloadStatus

fun GifResponseBody.GifData.toGifData(): GifData {
    return GifData(
        id = id.orEmpty(),
        gifUrl = images?.previewGif?.url.orEmpty(),
        height = images?.previewGif?.height.orEmpty(),
        width = images?.previewGif?.width.orEmpty()
    )
}

fun GifResponseBody.toDomain(): GifModel {
    return GifModel(
        gifData = gifData.map { it.toGifData() }
    )
}

fun GiphyEntity.toDomain(): GifDownloadModel =
    GifDownloadModel(
        gifId = gifId,
        downloadId = downloadId,
        gifName = gifName,
        gifUrl = gifUrl,
        gifUri = gifUri,
        downloadStatus = downloadStatus.toDomain()
    )

fun List<GiphyEntity>.toDomain(): List<GifDownloadModel> =
    this.map { it.toDomain() }

fun GifDownloadModel.toEntity(): GiphyEntity =
    GiphyEntity(
        gifId = gifId,
        downloadId = downloadId,
        gifName = gifName,
        gifUrl = gifUrl,
        gifUri = gifUri,
        downloadStatus = downloadStatus.toData()
    )

fun DownloadStatus.toDomain() = when (this) {
    DownloadStatus.PENDING -> DomainDownloadStatus.PENDING
    DownloadStatus.DOWNLOADED -> DomainDownloadStatus.DOWNLOADED
    DownloadStatus.DELETED -> DomainDownloadStatus.DELETED
}

fun DomainDownloadStatus.toData() = when (this) {
    DomainDownloadStatus.PENDING -> DownloadStatus.PENDING
    DomainDownloadStatus.DOWNLOADED -> DownloadStatus.DOWNLOADED
    DomainDownloadStatus.DELETED -> DownloadStatus.DELETED
}