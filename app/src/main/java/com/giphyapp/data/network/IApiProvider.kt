package com.giphyapp.data.network

interface IApiProvider {

    fun <T> getApi(baseUrl: String, service: Class<T>): T
}