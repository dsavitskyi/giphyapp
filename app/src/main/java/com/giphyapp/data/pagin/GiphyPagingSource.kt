package com.giphyapp.data.pagin

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.giphyapp.data.database.dao.GiphyDao
import com.giphyapp.data.database.entity.DownloadStatus
import com.giphyapp.data.datasource.model.GifResponseBody
import com.giphyapp.data.datasource.remote.GiphyRemoteDataSource
import retrofit2.HttpException
import java.io.IOException

class GiphyPagingSource (
    private val remoteDataSource: GiphyRemoteDataSource,
    private val gifDatabase: GiphyDao,
    private val searchQuery: String
): PagingSource<Int, GifResponseBody.GifData>() {

    private companion object {
        const val INITIAL_PAGE_INDEX = 0
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GifResponseBody.GifData> {
        val itemPosition = params.key ?: INITIAL_PAGE_INDEX

        return try {
            val offset = itemPosition * params.loadSize
            val gifResponse = remoteDataSource.getGifPackage(searchQuery, offset, params.loadSize)
            val deletedGifs = gifDatabase.getAllGifsByStatus(DownloadStatus.DELETED)

            val deletedGifsIds = deletedGifs.map { it.gifId }
            val gifs = gifResponse.gifData

            val filteredGifs = gifs.filter { gif ->
               !deletedGifsIds.contains(gif.id)
            }

            LoadResult.Page(
                data = filteredGifs,
                prevKey = if (itemPosition == INITIAL_PAGE_INDEX) null else itemPosition - 1,
                nextKey = if (gifs.isEmpty()) null else itemPosition + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        } catch (exception: Exception) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, GifResponseBody.GifData>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}