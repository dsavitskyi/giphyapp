package com.giphyapp.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.giphyapp.data.database.dao.GiphyDao
import com.giphyapp.data.datasource.remote.GiphyRemoteDataSource
import com.giphyapp.data.mapper.toData
import com.giphyapp.data.mapper.toDomain
import com.giphyapp.data.mapper.toEntity
import com.giphyapp.data.mapper.toGifData
import com.giphyapp.data.pagin.GiphyPagingSource
import com.giphyapp.domain.extentions.executeSafeOrNull
import com.giphyapp.domain.model.DownloadStatus
import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.model.GifDownloadModel
import com.giphyapp.domain.repository.IGiphyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GiphyRepository @Inject constructor(
    private val remoteDataSource: GiphyRemoteDataSource,
    private val gifDatabase: GiphyDao
): IGiphyRepository {

    override suspend fun getGifPackage(searchQuery: String, pageSize: Int): Flow<PagingData<GifData>> {
        return Pager(
            PagingConfig(
                pageSize = pageSize,
                enablePlaceholders = false,
                initialLoadSize = pageSize
            ), pagingSourceFactory = { GiphyPagingSource(remoteDataSource, gifDatabase, searchQuery) }
        ).flow.map { pagingData ->
            pagingData.map { it.toGifData() }
        }
    }

    override suspend fun saveDownloadStatus(gidDownloadModel: GifDownloadModel) {
        executeSafeOrNull { gifDatabase.insert(gidDownloadModel.toEntity()) }
    }

    override suspend fun updateDownloadStatus(downloadId: Long, fileName: String, gifUri: String) {
        executeSafeOrNull {
            gifDatabase.updateGifDownloadStatus(
                uri = gifUri,
                gifName = fileName,
                status = DownloadStatus.DOWNLOADED.toData(),
                downloadId = downloadId
            )
        }
    }

    override suspend fun getAllLocalStoredGifsByStatus(status: DownloadStatus): List<GifDownloadModel> {
        return executeSafeOrNull {
            gifDatabase.getAllGifsByStatus(status.toData()).map { it.toDomain() }
        } ?: listOf()
    }

    override suspend fun setGifSDeleteStatus(gifId: String) {
        executeSafeOrNull { gifDatabase.setGifSDeleteStatus(gifId) }
    }

    override fun getAllDownloadedGifs(): Flow<List<GifDownloadModel>> {
        return gifDatabase.getAllDownloadedGifs().map { it.toDomain() }
    }
}