package com.giphyapp.di

import android.content.Context
import com.giphyapp.di.annotations.ApplicationContext
import com.giphyapp.di.modules.activity.ActivityBindingModule
import com.giphyapp.di.modules.activity.MainActivityModule
import com.giphyapp.di.modules.database.DatabaseModule
import com.giphyapp.di.modules.managers.ManagersModule
import com.giphyapp.di.modules.receiver.BroadcastReceiverModule
import com.giphyapp.di.modules.remote.ApiModule
import com.giphyapp.di.modules.repository.RepositoryModule
import com.giphyapp.di.modules.viewmodel.ViewModelModule
import com.giphyapp.presentation.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        MainActivityModule::class,
        ApiModule::class,
        RepositoryModule::class,
        DatabaseModule::class,
        ManagersModule::class,
        BroadcastReceiverModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {


    @Component.Factory
    interface Factory {

        fun create(@BindsInstance @ApplicationContext context: Context): AppComponent
    }
}