package com.giphyapp.di.modules.activity

import androidx.lifecycle.ViewModel
import com.giphyapp.di.modules.viewmodel.DetailViewModelModule
import com.giphyapp.di.modules.viewmodel.GiphyViewModelModule
import com.giphyapp.di.modules.viewmodel.ViewModelKey
import com.giphyapp.di.scope.FragmentScope
import com.giphyapp.presentation.MainViewModel
import com.giphyapp.presentation.screens.detail.DetailFragment
import com.giphyapp.presentation.screens.home.GiphyFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(model: MainViewModel): ViewModel

    @FragmentScope
    @ContributesAndroidInjector(modules = [GiphyViewModelModule::class])
    fun giphyFragment(): GiphyFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [DetailViewModelModule::class])
    fun detailFragment(): DetailFragment
}