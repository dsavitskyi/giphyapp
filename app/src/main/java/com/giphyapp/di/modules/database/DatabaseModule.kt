package com.giphyapp.di.modules.database

import android.content.Context
import androidx.room.Room
import com.giphyapp.data.database.GiphyDatabase
import com.giphyapp.data.database.dao.GiphyDao
import com.giphyapp.di.annotations.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): GiphyDatabase {
        return Room.databaseBuilder(context, GiphyDatabase::class.java, GiphyDatabase.NAME).build()
    }


    @Provides
    @Singleton
    fun provideGiphyDao(database: GiphyDatabase): GiphyDao {
        return database.getGiphyDao()
    }
}