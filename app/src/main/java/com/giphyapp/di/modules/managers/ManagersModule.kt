package com.giphyapp.di.modules.managers

import com.giphyapp.domain.managers.FilePathProvider
import com.giphyapp.domain.managers.IGifsDownloadManager
import com.giphyapp.presentation.managers.DownloadsFilePathProvider
import com.giphyapp.presentation.managers.GifsDownloadManager
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ManagersModule {

    @Binds
    @Singleton
    fun bindGifsDownloadManager(manager: GifsDownloadManager): IGifsDownloadManager

    @Binds
    @Singleton
    fun bindDownloadsFilePathProvider(provider: DownloadsFilePathProvider): FilePathProvider

}