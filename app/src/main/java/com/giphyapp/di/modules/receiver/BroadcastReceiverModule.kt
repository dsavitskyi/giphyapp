package com.giphyapp.di.modules.receiver

import com.giphyapp.presentation.receivers.DownloadCompleteReceiver
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BroadcastReceiverModule {

    @ContributesAndroidInjector
    abstract fun contributesDownloadCompleteReceiver() : DownloadCompleteReceiver
}