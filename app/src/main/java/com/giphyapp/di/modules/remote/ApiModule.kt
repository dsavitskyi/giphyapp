package com.giphyapp.di.modules.remote

import com.giphyapp.data.network.ApiProvider
import com.giphyapp.data.network.IApiProvider
import dagger.Binds
import dagger.Module
import dagger.Reusable

@Module
interface ApiModule {

    @Binds
    @Reusable
    fun bindApiProviderModule(helper: ApiProvider): IApiProvider
}