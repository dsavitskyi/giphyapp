package com.giphyapp.di.modules.repository

import com.giphyapp.data.repository.GiphyRepository
import com.giphyapp.domain.repository.IGiphyRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindGiphyRepository(repo: GiphyRepository): IGiphyRepository

}