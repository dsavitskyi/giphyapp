package com.giphyapp.di.modules.viewmodel

import androidx.lifecycle.ViewModel
import com.giphyapp.presentation.screens.detail.DetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface DetailViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    fun bindDetailViewModel(model: DetailViewModel): ViewModel
}