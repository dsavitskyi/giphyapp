package com.giphyapp.di.modules.viewmodel

import androidx.lifecycle.ViewModel
import com.giphyapp.presentation.screens.home.GiphyViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface GiphyViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(GiphyViewModel::class)
    fun bindGiphyViewModel(model: GiphyViewModel): ViewModel
}