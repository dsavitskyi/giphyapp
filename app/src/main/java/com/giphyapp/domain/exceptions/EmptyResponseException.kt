package com.giphyapp.domain.exceptions

class EmptyResponseException : RuntimeException()