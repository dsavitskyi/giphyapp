package com.giphyapp.domain.extentions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

fun ImageView.loadWithCacheGlide(
    url: String?,
    placeholderResId: Int? = null
) {
    Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).also { glideRequest ->
        placeholderResId?.let {
            glideRequest.placeholder(it)
        }
        glideRequest.into(this)
    }
}