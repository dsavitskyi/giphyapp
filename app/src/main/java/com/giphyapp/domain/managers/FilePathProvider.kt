package com.giphyapp.domain.managers

import java.io.File

interface FilePathProvider {

    fun getGifsFolder(folderName: String): File?

    fun getGifFilePath(folderName: String, gifName: String): String?

    fun getGifFile(parentFolder: File, fileName: String): File
}