package com.giphyapp.domain.managers

import android.app.DownloadManager

interface IGifsDownloadManager {

    val downloadManager: DownloadManager

    suspend fun downloadFile(url: String): Long

    suspend fun saveDownloadFileStatus(gifId: String, gifUrl: String)

    suspend fun updateDownloadStatus(downloadId: Long, fileName: String, gifUri: String)
}