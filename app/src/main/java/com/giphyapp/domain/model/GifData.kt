package com.giphyapp.domain.model

data class GifData(
    val id: String,
    val gifUrl: String,
    val height: String,
    val width: String
)