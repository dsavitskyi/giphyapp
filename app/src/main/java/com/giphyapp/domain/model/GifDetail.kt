package com.giphyapp.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GifDetail(
    val id: String,
    val gifUrl: String?,
    val gifUri: String?
): Parcelable