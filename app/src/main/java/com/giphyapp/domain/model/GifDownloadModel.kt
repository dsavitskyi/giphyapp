package com.giphyapp.domain.model

data class GifDownloadModel(
    val gifId: String,
    val downloadId: Long,
    val gifName: String = "",
    val gifUrl: String,
    val gifUri: String = "",
    val downloadStatus: DownloadStatus
)

enum class DownloadStatus {
    PENDING,
    DOWNLOADED,
    DELETED
}