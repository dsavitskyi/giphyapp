package com.giphyapp.domain.model

data class GifModel(
    val gifData: List<GifData>
)