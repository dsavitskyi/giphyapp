package com.giphyapp.domain.repository

import androidx.paging.PagingData
import com.giphyapp.domain.model.DownloadStatus
import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.model.GifDownloadModel
import kotlinx.coroutines.flow.Flow

interface IGiphyRepository {

    suspend fun getGifPackage(searchQuery: String, pageSize: Int): Flow<PagingData<GifData>>

    suspend fun saveDownloadStatus(gidDownloadModel: GifDownloadModel)

    suspend fun updateDownloadStatus(downloadId: Long, fileName: String, gifUri: String)

    suspend fun getAllLocalStoredGifsByStatus(status: DownloadStatus): List<GifDownloadModel>

    suspend fun setGifSDeleteStatus(gifId: String)

    fun getAllDownloadedGifs(): Flow<List<GifDownloadModel>>
}