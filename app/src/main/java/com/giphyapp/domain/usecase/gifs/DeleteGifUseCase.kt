package com.giphyapp.domain.usecase.gifs

import com.giphyapp.domain.repository.IGiphyRepository
import com.giphyapp.domain.usecase.UseCase
import javax.inject.Inject

class DeleteGifUseCase @Inject constructor(
    private val giphyRepository: IGiphyRepository,
): UseCase<DeleteGifUseCase.DeleteGifParams, Unit> {

    override suspend fun execute(params: DeleteGifParams) {
        giphyRepository.setGifSDeleteStatus(params.gifId)
    }

    data class DeleteGifParams(val gifId: String): UseCase.Params
}