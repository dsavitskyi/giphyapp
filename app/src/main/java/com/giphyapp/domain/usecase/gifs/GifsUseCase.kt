package com.giphyapp.domain.usecase.gifs

import androidx.paging.PagingData
import androidx.paging.map
import com.giphyapp.domain.managers.IGifsDownloadManager
import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.repository.IGiphyRepository
import com.giphyapp.domain.usecase.UseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GifsUseCase @Inject constructor(
    private val giphyRepository: IGiphyRepository,
    private val downloadManager: IGifsDownloadManager
): UseCase<GifsUseCase.GifParams, Flow<PagingData<GifData>>> {

    override suspend fun execute(params: GifParams): Flow<PagingData<GifData>> {
        return giphyRepository.getGifPackage(params.searchQuery, GIFS_RESULTS_LIMIT).map { pagingData ->
            pagingData.map {
                downloadGifImage(it.id, it.gifUrl)
                it
            }
        }
    }

    private suspend fun downloadGifImage(gifId: String, url: String) {
        downloadManager.saveDownloadFileStatus(gifId, url)
    }

    data class GifParams(val searchQuery: String): UseCase.Params

    companion object {
        const val GIFS_RESULTS_LIMIT = 50
    }
}