package com.giphyapp.domain.usecase.gifs

import com.giphyapp.domain.model.GifDownloadModel
import com.giphyapp.domain.repository.IGiphyRepository
import com.giphyapp.domain.usecase.EmptyParamsFlowUseCase
import com.giphyapp.domain.usecase.FlowUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalGifsUseCase @Inject constructor(
    private val giphyRepository: IGiphyRepository,
): EmptyParamsFlowUseCase<List<GifDownloadModel>> {

    override fun execute(params: FlowUseCase.EmptyParams): Flow<List<GifDownloadModel>> {
        return giphyRepository.getAllDownloadedGifs()
    }
}