package com.giphyapp.presentation.managers

import android.content.Context
import com.giphyapp.di.annotations.ApplicationContext
import com.giphyapp.domain.managers.FilePathProvider
import java.io.File
import javax.inject.Inject

class DownloadsFilePathProvider @Inject constructor(
    @ApplicationContext private val context: Context
): FilePathProvider {

    override fun getGifsFolder(folderName: String): File? {
        val rootDirPath = context.getExternalFilesDir(null) ?: return null
        val filePath = rootDirPath.path.plus(File.separator).plus(folderName)
        return File(filePath)
    }

    override fun getGifFilePath(folderName: String, gifName: String): String? {
        val rootDirPath = context.getExternalFilesDir(null) ?: return null
        return rootDirPath.path.plus(File.separator).plus(folderName).plus(File.separator).plus(gifName)
    }

    override fun getGifFile(parentFolder: File, fileName: String): File {
        return File(parentFolder, fileName)
    }
}