package com.giphyapp.presentation.managers

import android.app.DownloadManager
import android.content.Context
import androidx.core.net.toUri
import com.giphyapp.di.annotations.ApplicationContext
import com.giphyapp.domain.managers.FilePathProvider
import com.giphyapp.domain.managers.IGifsDownloadManager
import com.giphyapp.domain.model.DownloadStatus
import com.giphyapp.domain.model.GifDownloadModel
import com.giphyapp.domain.repository.IGiphyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GifsDownloadManager @Inject constructor(
    @ApplicationContext private val context: Context,
    private val filePathProvider: FilePathProvider,
    private val giphyRepository: IGiphyRepository
): IGifsDownloadManager {

    private val _downloadManager: DownloadManager = context.getSystemService(DownloadManager::class.java)
    override val downloadManager: DownloadManager
        get() = _downloadManager

    override suspend fun downloadFile(url: String): Long = withContext(Dispatchers.IO) {
        var downloadId = -1L
        val fileName = url.substringBeforeLast("/").substringAfterLast("/")
        val fileFolder = filePathProvider.getGifsFolder(GIFS_FILES_DIRECTORY)
        fileFolder?.let { folder ->
            val currentFile = filePathProvider.getGifFile(folder, fileName)
            if (!currentFile.exists()) {
                val request = DownloadManager.Request(url.toUri())
                    .setMimeType("image/gif")
                    .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN)
                    .setDestinationInExternalFilesDir(context, GIFS_FILES_DIRECTORY, fileName)

                downloadId = _downloadManager.enqueue(request)
            }
        }
        return@withContext downloadId
    }

    override suspend fun saveDownloadFileStatus(gifId: String, gifUrl: String) {
        val downloadId = downloadFile(gifUrl)
        if (downloadId != -1L) {
            val gifModel = GifDownloadModel(
                gifId = gifId,
                downloadId = downloadId,
                gifUrl = gifUrl,
                downloadStatus = DownloadStatus.PENDING
            )
            giphyRepository.saveDownloadStatus(gifModel)
        }
    }

    override suspend fun updateDownloadStatus(downloadId: Long, fileName: String, gifUri: String) {
        giphyRepository.updateDownloadStatus(downloadId, fileName, gifUri)
    }

    companion object {
        const val GIFS_FILES_DIRECTORY = "gif_assets"
    }
}