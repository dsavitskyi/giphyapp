package com.giphyapp.presentation.mapper

import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.model.GifDetail
import com.giphyapp.domain.model.GifDownloadModel

fun GifData.toDetail(): GifDetail =
    GifDetail(
        id = id,
        gifUrl = gifUrl,
        gifUri = null
    )

fun List<GifData>.toDetailList(): List<GifDetail> =
    this.map { it.toDetail() }

fun GifDownloadModel.toDownloadDetail(): GifDetail =
    GifDetail(
        id = gifId,
        gifUrl = null,
        gifUri = gifUri
    )

fun List<GifDownloadModel>.toDownloadDetailList(): List<GifDetail> =
    this.map { it.toDownloadDetail() }