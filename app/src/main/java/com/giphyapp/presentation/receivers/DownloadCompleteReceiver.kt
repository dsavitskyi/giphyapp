package com.giphyapp.presentation.receivers

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.database.Cursor
import com.giphyapp.domain.managers.IGifsDownloadManager
import dagger.android.DaggerBroadcastReceiver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject

class DownloadCompleteReceiver: DaggerBroadcastReceiver() {

    @Inject
    lateinit var gifsDownloadManager: IGifsDownloadManager

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        if (intent?.action == "android.intent.action.DOWNLOAD_COMPLETE") {
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1L)
            if (id != -1L) {
                val query = DownloadManager.Query()
                query.setFilterById(id)
                val cursor: Cursor? = gifsDownloadManager.downloadManager.query(query)
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        val status: Int = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)?: 0)
                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            val filePath: String =
                                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)?: 0)
                            val fileName = filePath.substringAfterLast("/")
                            CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
                                gifsDownloadManager.updateDownloadStatus(id, fileName, filePath)
                            }
                        }
                    }
                    cursor.close()
                }
            }
        }
    }
}