package com.giphyapp.presentation.screens.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.giphyapp.databinding.FragmentDetailBinding
import com.giphyapp.domain.extentions.collectWithLifecycle
import com.giphyapp.presentation.ViewBindingFragment
import com.giphyapp.presentation.screens.detail.adapter.GifDetailAdapter
import javax.inject.Inject

class DetailFragment: ViewBindingFragment<FragmentDetailBinding>(FragmentDetailBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: DetailViewModel by viewModels(factoryProducer = { factory })
    private val navArgs: DetailFragmentArgs by navArgs()

    private val gifDetailAdapter by lazy { GifDetailAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupGifsAdapter()
        setListeners()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.gifInitialPosition.collectWithLifecycle(
            lifecycleOwner = viewLifecycleOwner,
            minActiveState = Lifecycle.State.CREATED
        ) { position ->
            binding.rvGifs.scrollToPosition(position)
        }
    }

    private fun setListeners() {
        binding.rvGifs.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val position = getCurrentItem()
                    viewModel.updateCurrentPosition(position)
                }
            }
        })
    }

    private fun getCurrentItem() =
        (binding.rvGifs.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


    private fun setupGifsAdapter() {
        val pagerSnapHelper = PagerSnapHelper()
        with(binding.rvGifs) {
            adapter = gifDetailAdapter
            pagerSnapHelper.attachToRecyclerView(this)
        }
        val gifs = navArgs.gifs.toMutableList()
        gifDetailAdapter.submitList(gifs)
        val currentIndex = gifs.indexOfFirst { it.id == navArgs.gifId }
        if (currentIndex != -1) {
            viewModel.setScrollPosition(currentIndex)
        }
    }
}