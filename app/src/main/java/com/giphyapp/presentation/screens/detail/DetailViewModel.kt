package com.giphyapp.presentation.screens.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailViewModel @Inject constructor() : ViewModel() {

    private val _gifInitialPosition = MutableSharedFlow<Int>(replay = 1)
    val gifInitialPosition: SharedFlow<Int> = _gifInitialPosition

    private var currentScrollPosition = -1

    fun setScrollPosition(position: Int) {
        viewModelScope.launch {
            val updatedPosition = if (currentScrollPosition == -1) position else currentScrollPosition
            _gifInitialPosition.emit(updatedPosition)
        }
    }

    fun updateCurrentPosition(position: Int) {
        currentScrollPosition = position
    }
}