package com.giphyapp.presentation.screens.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.giphyapp.R
import com.giphyapp.databinding.ItemDetailBinding
import com.giphyapp.domain.extentions.loadWithCacheGlide
import com.giphyapp.domain.model.GifDetail

class GifDetailAdapter: ListAdapter<GifDetail, GifDetailAdapter.ItemViewHolder>(DIFF_UTIL) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemDetailBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind() {
            val currentItem = getItem(bindingAdapterPosition)
            itemBinding.ivGif.loadWithCacheGlide(
                currentItem?.gifUrl?: currentItem?.gifUri,
                R.drawable.ic_gif_placeholder
            )
        }
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<GifDetail>() {

            override fun areItemsTheSame(
                oldItem: GifDetail,
                newItem: GifDetail
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: GifDetail,
                newItem: GifDetail
            ): Boolean = oldItem == newItem
        }
    }
}