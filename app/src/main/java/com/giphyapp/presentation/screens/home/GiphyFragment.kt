package com.giphyapp.presentation.screens.home

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import com.giphyapp.R
import com.giphyapp.databinding.FragmentGiphyBinding
import com.giphyapp.domain.extentions.collectWithLifecycle
import com.giphyapp.domain.extentions.hideSoftKeyboard
import com.giphyapp.domain.extentions.toastSh
import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.model.GifDetail
import com.giphyapp.domain.model.GifDownloadModel
import com.giphyapp.presentation.ViewBindingFragment
import com.giphyapp.presentation.mapper.toDetailList
import com.giphyapp.presentation.mapper.toDownloadDetailList
import com.giphyapp.presentation.screens.home.adapter.GifAdapter
import com.giphyapp.presentation.screens.home.adapter.GifLocalStorageAdapter
import com.giphyapp.presentation.utils.isNetworkAvailable
import javax.inject.Inject

class GiphyFragment: ViewBindingFragment<FragmentGiphyBinding>(FragmentGiphyBinding::inflate),
    GifAdapter.IOnItemClickListener, GifLocalStorageAdapter.IOnItemClickListener {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: GiphyViewModel by viewModels(factoryProducer = { factory })

    private val gifsAdapter by lazy {
        GifAdapter().apply {
            gifClickListener = this@GiphyFragment
        }
    }

    private val gifsLocalAdapter by lazy {
        GifLocalStorageAdapter().apply {
            localGifClickListener = this@GiphyFragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRequiredGifs()
        setListeners()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.gifsFlow.collectWithLifecycle(viewLifecycleOwner) {
            gifsAdapter.submitData(it)
        }
        viewModel.clearGifs.collectWithLifecycle(
            lifecycleOwner = viewLifecycleOwner,
            minActiveState = Lifecycle.State.CREATED
        ) {
            gifsAdapter.submitData(PagingData.empty())
        }
        viewModel.localGifsFlow.collectWithLifecycle(viewLifecycleOwner) { state ->
            when(state) {
                is GiphyViewModel.LocalGifsState.Loading -> {
                    setLoadProgressVisibility(true)
                }
                is GiphyViewModel.LocalGifsState.Success -> {
                    setLoadProgressVisibility(false)
                    gifsLocalAdapter.submitList(state.gifs)
                }
                is GiphyViewModel.LocalGifsState.Empty -> {
                    setLoadProgressVisibility(false)
                    binding.tvNoResults.isVisible = true
                }
            }
        }
    }

    private fun setListeners() {
        gifsAdapter.addLoadStateListener {
            val isListEmpty = it.refresh is LoadState.NotLoading && gifsAdapter.itemCount == 0
            binding.tvNoResults.isVisible = isListEmpty
            setLoadProgressVisibility(it.source.refresh is LoadState.Loading)

            val errorState = it.source.append as? LoadState.Error
                ?: it.source.prepend as? LoadState.Error
                ?: it.append as? LoadState.Error
                ?: it.prepend as? LoadState.Error
            errorState?.let { error ->
                toastSh(error.error.message?: getString(R.string.error_message))
            }
        }
        binding.etSearch.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                viewModel.clearGifData()
                viewModel.getGifs(
                    binding.etSearch.text.toString().trim()
                )
                requireActivity().hideSoftKeyboard()
                return@OnKeyListener true
            }
            false
        })
        binding.etSearch.doAfterTextChanged {
            binding.tilSearch.isEndIconVisible = it.toString().isNotEmpty()
        }
        binding.tilSearch.setEndIconOnClickListener {
            binding.etSearch.setText("")
            requestRandomGifs()
        }
    }

    private fun setLoadProgressVisibility(isVisible: Boolean) {
        binding.pbLoading.isVisible = isVisible
    }

    private fun requestRandomGifs() {
        viewModel.clearGifData()
        viewModel.getGifs()
    }

    private fun setupGifsAdapter() {
        binding.rvGifs.adapter = gifsAdapter
    }

    private fun setupLocalGifsAdapter() {
        binding.rvGifs.adapter = gifsLocalAdapter
        viewModel.getLocalGifs()
    }

    private fun setupRequiredGifs() {
        if (isNetworkAvailable(requireContext())) {
            setupGifsAdapter()
        } else {
            setupLocalGifsAdapter()
        }
    }

    private fun navigateToGifDetail(gifId: String, gifList: List<GifDetail>) {
        findNavController().navigate(GiphyFragmentDirections.actionGiphyFragmentToDetailFragment(
            gifs = gifList.toTypedArray(),
            gifId = gifId
        ))
    }

    override fun onOpenGifClick(item: GifData?) {
        val list = gifsAdapter.snapshot().items
        val gifList = list.toDetailList()
        navigateToGifDetail(item?.id.orEmpty(), gifList)
    }

    override fun onDeleteGifClick(item: GifData?) {
        viewModel.deleteGif(item?.id.orEmpty())
        gifsAdapter.refresh()
    }

    override fun onOpenLocalGifClick(item: GifDownloadModel) {
        val list = gifsLocalAdapter.currentList
        val gifList = list.toDownloadDetailList()
        navigateToGifDetail(item.gifId, gifList)
    }

    override fun onDeleteLocalGifClick(item: GifDownloadModel) {
        viewModel.deleteGif(item.gifId)
    }
}