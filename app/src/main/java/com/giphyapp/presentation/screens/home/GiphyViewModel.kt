package com.giphyapp.presentation.screens.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.giphyapp.domain.model.GifData
import com.giphyapp.domain.model.GifDownloadModel
import com.giphyapp.domain.usecase.gifs.DeleteGifUseCase
import com.giphyapp.domain.usecase.gifs.GifsUseCase
import com.giphyapp.domain.usecase.gifs.LocalGifsUseCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class GiphyViewModel @Inject constructor(
    private val gifsUseCase: GifsUseCase,
    private val localGifsUseCase: LocalGifsUseCase,
    private val deleteGifUseCase: DeleteGifUseCase
) : ViewModel() {

    private val _gifsFlow = MutableSharedFlow<PagingData<GifData>>(replay = 1)
    val gifsFlow: SharedFlow<PagingData<GifData>> = _gifsFlow

    private val _localGifsFlow = MutableSharedFlow<LocalGifsState>(replay = 1)
    val localGifsFlow: SharedFlow<LocalGifsState> = _localGifsFlow

    private val _clearGifs = MutableSharedFlow<Unit>(replay = 1)
    val clearGifs: SharedFlow<Unit> = _clearGifs

    private var job: Job? = null

    init {
        getGifs()
    }

    fun getGifs(searchQuery: String = DEFAULT_SEARCH_QUERY) {
        job?.cancel()
        viewModelScope.launch(Job().also { job = it }) {
            val result = gifsUseCase.execute(GifsUseCase.GifParams(searchQuery)).cachedIn(viewModelScope)
            result.collectLatest { pagingData ->
                _gifsFlow.emit(pagingData)
            }
        }
    }

    fun getLocalGifs() {
        job?.cancel()
        viewModelScope.launch(Job().also { job = it }) {
            _localGifsFlow.emit(LocalGifsState.Loading)
            localGifsUseCase.execute().collectLatest { result ->
                _localGifsFlow.emit(
                    if (result.isEmpty()) LocalGifsState.Empty else LocalGifsState.Success(result)
                )
            }
        }
    }

    fun clearGifData() {
        viewModelScope.launch {
            _clearGifs.emit(Unit)
        }
    }

    fun deleteGif(gifId: String) {
        viewModelScope.launch {
            deleteGifUseCase.execute(DeleteGifUseCase.DeleteGifParams(gifId))
        }
    }

    sealed class LocalGifsState {
        object Loading: LocalGifsState()
        data class Success(val gifs: List<GifDownloadModel>): LocalGifsState()
        object Empty: LocalGifsState()
    }

    companion object {
        private const val DEFAULT_SEARCH_QUERY = "random"
    }
}