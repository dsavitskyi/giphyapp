package com.giphyapp.presentation.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.giphyapp.R
import com.giphyapp.databinding.ItemGifBinding
import com.giphyapp.domain.extentions.loadWithCacheGlide
import com.giphyapp.domain.model.GifData

class GifAdapter: PagingDataAdapter<GifData, GifAdapter.ItemViewHolder>(DIFF_UTIL) {

    var gifClickListener: IOnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemGifBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemGifBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.ivGif.setOnClickListener {
                gifClickListener?.onOpenGifClick(getItem(bindingAdapterPosition))
            }
            itemBinding.ivDelete.setOnClickListener {
                gifClickListener?.onDeleteGifClick(getItem(bindingAdapterPosition))
            }
        }

        fun bind() {
            val currentItem = getItem(bindingAdapterPosition)
            itemBinding.ivGif.loadWithCacheGlide(currentItem?.gifUrl, R.drawable.ic_gif_placeholder)
        }
    }

    interface IOnItemClickListener {
        fun onOpenGifClick(item: GifData?)
        fun onDeleteGifClick(item: GifData?)
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<GifData>() {

            override fun areItemsTheSame(
                oldItem: GifData,
                newItem: GifData
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: GifData,
                newItem: GifData
            ): Boolean = oldItem == newItem
        }
    }
}