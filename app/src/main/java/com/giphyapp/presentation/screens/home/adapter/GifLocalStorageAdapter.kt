package com.giphyapp.presentation.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.giphyapp.R
import com.giphyapp.databinding.ItemGifBinding
import com.giphyapp.domain.extentions.loadWithCacheGlide
import com.giphyapp.domain.model.GifDownloadModel

class GifLocalStorageAdapter: ListAdapter<GifDownloadModel, GifLocalStorageAdapter.ItemViewHolder>(DIFF_UTIL) {

    var localGifClickListener: IOnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemGifBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemGifBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.ivGif.setOnClickListener {
                localGifClickListener?.onOpenLocalGifClick(getItem(bindingAdapterPosition))
            }
            itemBinding.ivDelete.setOnClickListener {
                localGifClickListener?.onDeleteLocalGifClick(getItem(bindingAdapterPosition))
            }
        }

        fun bind() {
            val currentItem = getItem(bindingAdapterPosition)
            itemBinding.ivGif.loadWithCacheGlide(currentItem?.gifUri, R.drawable.ic_gif_placeholder)
        }
    }

    interface IOnItemClickListener {
        fun onOpenLocalGifClick(item: GifDownloadModel)
        fun onDeleteLocalGifClick(item: GifDownloadModel)
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<GifDownloadModel>() {

            override fun areItemsTheSame(
                oldItem: GifDownloadModel,
                newItem: GifDownloadModel
            ): Boolean = oldItem.gifId == newItem.gifId

            override fun areContentsTheSame(
                oldItem: GifDownloadModel,
                newItem: GifDownloadModel
            ): Boolean = oldItem == newItem
        }
    }
}